extern crate argonaut;
extern crate byteorder;

mod instructions;
mod simulator;

use std::env;
use argonaut::{ArgDef, parse, ParseError};
use byteorder::{ReadBytesExt, WriteBytesExt, LE};
use std::process;
use std::fs::File;
use std::io::{Read, Cursor, Write};
use std::error::Error;
use instructions::{Instr, Reg};
use simulator::{Interpreter, Signal};
use std::iter::{self};

fn main() {
    if let Some(exit_code) = argonaut_main() {
        process::exit(exit_code);
    }
}

type Registers = [i32; 32];


const IO_ERROR: i32 = 1;
const RT_ERROR: i32 = 2;
const DC_ERROR: i32 = 3;

fn format_registers(registers: &Registers) -> String {
    use std::fmt::Write;
    let mut s = String::new();
    for (i, &value) in registers.iter().enumerate() {
        let register = Reg::decode(i as u32).unwrap();
        writeln!(s, "{} {}", register.name(), value).unwrap();
    }
    s
}

#[allow(unused)]
fn load_file_text(source_file: &str) -> Result<String, i32> {
    let mut sf = match File::open(&source_file) {
        Ok(sf) => sf,
        Err(err) => {
            println!("Could not open source file: {}", err.description());
            return Err(IO_ERROR);
        }
    };
    let mut source = String::new();
    if let Err(err) = sf.read_to_string(&mut source) {
        println!("Could not read source file: {}", err.description());
        return Err(IO_ERROR);
    }
    Ok(source)
}

fn write_file_text(file: &str, text: &str) -> Result<(), i32> {
    let mut f = match File::create(&file) {
        Ok(sf) => sf,
        Err(err) => {
            println!("Could not create file {}: {}", file, err.description());
            return Err(IO_ERROR);
        }
    };

    match f.write_all(text.as_bytes()) {
        Ok(_) => Ok(()),
        Err(err) => {
            println!("Could not write to {}: {}", file, err.description());
            Err(IO_ERROR)
        }
    }
}

fn load_file_opcodes(source_file: &str) -> Result<Vec<u32>, i32> {
    let mut sf = match File::open(&source_file) {
        Ok(sf) => sf,
        Err(err) => {
            println!("Could not open source file: {}", err.description());
            return Err(IO_ERROR);
        }
    };
    let mut bytes = Vec::new();
    if let Err(err) = sf.read_to_end(&mut bytes) {
        println!("Could not read source file: {}", err.description());
        return Err(IO_ERROR);
    }
    let mut codes = Vec::new();
    let nof_codes = bytes.len() / 4;
    let mut cursor = Cursor::new(bytes);
    for i in 0..nof_codes {
        match cursor.read_u32::<LE>() {
            Ok(code) => codes.push(code),
            Err(err) => {
                eprintln!("Could not read code at byte {} as u32: {}", 
                    i*4, err.description());
                return Err(IO_ERROR);
            }
        }
    }
    Ok(codes)
}

fn dump_registers(registers: &Registers, file: &str) -> Result<(), i32> {
    let mut f = match File::create(&file) {
        Ok(sf) => sf,
        Err(err) => {
            eprintln!("Could not create file {}: {}", file, err.description());
            return Err(IO_ERROR);
        }
    };
    for &reg in registers.iter() {
        match f.write_i32::<LE>(reg) {
            Ok(_) => {},
            Err(err) => {
                eprintln!("Could not write register to output file: {}", 
                    err.description());
                return Err(IO_ERROR);
            }
        }
    }
    Ok(())
}

fn run(codes: &[u32]) -> Result<(Option<i32>, Registers), i32> {
    let mut program = Vec::new();
    let mut decode_error = false;
    eprintln!("Program:");
    for (i, &code) in codes.iter().enumerate() {
        match Instr::decode(code) {
            Ok(instr) => {
                eprintln!("  {:04} {:?}", i*4, instr);
                program.push(instr);
            },
            Err(msg) => {
                eprintln!("  {:04} Decode error: {}", i*4, msg);
                decode_error = true;
            }
        }
    }
    eprintln!("");

    if decode_error {
        return Err(DC_ERROR);
    }

    // allocate one megabyte of memory.
    let memory = iter::repeat(0).take(1_048_476).collect::<Vec<_>>();
    let mut interpreter = Interpreter::new(&program, memory);
    while ! interpreter.is_done() {
        match interpreter.advance() {
            Err(message) => {
                eprintln!("Error: {}", message);
                return Err(RT_ERROR);
            }
            Ok(Signal::EBreak) => {
                // TODO: Do something :p
                // print info
                // wait for input or something
            }
            Ok(Signal::Exit(code)) => {
                return Ok((Some(code as i32), interpreter.registers()));
            }
            Ok(Signal::Null) => {}
            Ok(Signal::End) => break,
        }
    }
    Ok((None, interpreter.registers()))
}


fn argonaut_main() -> Option<i32> {
    let args = env::args().skip(1).collect::<Vec<_>>();
    
    let mut source_file = String::new();
    let mut output_file: Option<String> = None;
    let mut dump_binary = false;
    
    match parse("whisky", &args, vec![        
        ArgDef::positional("file", &mut source_file)
            .help("
                A binary file containing 32bit-encoded RISC-V instructions from 
                the IM instruction subset (basic + mul/div).
            ")
        
        , ArgDef::setting("output-file", &mut output_file)
            .short("o")
            .help("
                A file to save the register state of the simulator 
                to after running the program.
            ")
        
        , ArgDef::flag("dump-binary", &mut dump_binary)
            .short("b")
            .help("
                Save the register state in a binary format, rather than 
                a textual one.
            ")
        
        , ArgDef::default_help("
            Runs a binary-encoded RISC-V file on a simulator.
        ") 
        .short("h"), 
    ]) {
        Ok(_) => {},
        Err(ParseError::Interrupted(_)) => return None,
        Err(_) => return Some(1),
    };
            
    let codes = match load_file_opcodes(&source_file) {
        Ok(text) => text,
        Err(code) => return Some(code),
    };
    
    let (retcode, registers) = match run(&codes) {
        Ok((retcode, registers)) => (retcode, registers),
        Err(retcode) => return Some(retcode),
    };
    
    if let Some(ref output_file) = output_file {
        if dump_binary {
            match dump_registers(&registers, output_file) {
                Ok(_) => {},
                Err(err_code) => return Some(err_code),
            }

        } else {
            let register_text = format_registers(&registers);
            match write_file_text(output_file, &register_text) {
                Ok(_) => {},
                Err(err_code) => return Some(err_code),
            }
        }
    }
    
    retcode
}
