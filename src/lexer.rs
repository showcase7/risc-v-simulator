
use std::iter::{Peekable};
use std::str::CharIndices;

use keywords::{Keyword, keywords};

pub enum TokenKind {
    Kw(Keyword),
    Eof,
}

pub struct Token {
    pub kind: TokenKind,
    pub start: usize,
    pub end: usize,
}


struct Lexer<'src> {
    text: &'str src,
    start: usize,
    chars: Peekable<CharIndices<'src>>,
    keywords: HashMap<String, TokenKind>,
}
impl<'src> Lexer<'src> {
    fn new(source_text: &'src str) -> Lexer<'src> {
        text: source_text,
        start: 0,
        chars: source_text.char_indices().peekable(),
        keywords: keywords(),
    }
}

