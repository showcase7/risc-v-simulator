use std::collections::HashMap;

pub enum Keyword { // ops, registers
    // Base integer instructions
    Add, Addw, Addi, Addiw,
    And, Andi,
    Auipc,
    Beq, Bge, Bgeu, Blt, Bltu, Bne,
    Csrrc, Srcrci, Csrrs, Srcrsi, Csrrw, Csrrwi,
    Ecall, Ebreak,
    //Fence, Fencei,
    Jal, Jalr,
    Lb, Lbu, Ld, Lh, Lhu, Lui, Lw, Lwu,
    Or, Ori,
    Sb, Sd, Sh, Sw,
    Sll, Sllw, Slli, Slliw, // <<
    Srl, Srlw, Srli, Srliw, // >>
    Sra, Sraw, Srai, Sraiw, // >> arithmetic
    Slt, Slti, Sltiu, // rd <- a < b? 1: 0
    Sub, Subw,
    Xor, Xori,
    Mul, Mulw, Mulh, Mulhsu, Mulhu,
    Div, Divw, Divu,
    Rem, Remw, Remu, Remuw,
    
    // Pseudoinstructions
    Beqz, Bneq,
    // fabs.s, fabs.d, fmvs, fmvd, fnegs, fnegd
    J, Jr,
    La, Li,
    Mv,
    Neg,
    Nop,
    Not,
    Ret,
    Seqz, Snez,
    
    // * Not implemented in the first ver *
    // Lots of FP stuff
    // Atomic ops
}


pub fn keywords() -> HashMap<&'static str, Keyword> {
    use self::Keyword::*;
    let mut map = HashMap::new();
    map.insert("add", Add);
    map.insert("addw", Addw);
    map.insert("addi", Addi);
    map.insert("addiw", Addiw);
    map.insert("and", And);
    map.insert("andi", Andi);
    map.insert("auipc", Auipc);
    map.insert("beq", Beq);
    map.insert("bge", Bge);
    map.insert("bgeu", Bgeu);
    map.insert("blt", Blt);
    map.insert("bltu", Bltu);
    map.insert("bne", Bne);
    map.insert("csrrc", Csrrc);
    map.insert("srcrci", Srcrci);
    map.insert("csrrs", Csrrs);
    map.insert("srcrsi", Srcrsi);
    map.insert("csrrw", Csrrw);
    map.insert("csrrwi", Csrrwi);
    map.insert("ecall", Ecall);
    map.insert("ebreak", Ebreak);
    map.insert("jal", Jal);
    map.insert("jalr", Jalr);
    map.insert("lb", Lb);
    map.insert("lbu", Lbu);
    map.insert("ld", Ld);
    map.insert("lh", Lh);
    map.insert("lhu", Lhu);
    map.insert("lui", Lui);
    map.insert("lw", Lw);
    map.insert("lwu", Lwu);
    map.insert("or", Or);
    map.insert("ori", Ori);
    map.insert("sb", Sb);
    map.insert("sd", Sd);
    map.insert("sh", Sh);
    map.insert("sw", Sw);
    map.insert("sll", Sll);
    map.insert("sllw", Sllw);
    map.insert("slli", Slli);
    map.insert("slliw", Slliw);
    map.insert("srl", Srl);
    map.insert("srlw", Srlw);
    map.insert("srli", Srli);
    map.insert("srliw", Srliw);
    map.insert("sra", Sra);
    map.insert("sraw", Sraw);
    map.insert("srai", Srai);
    map.insert("sraiw", Sraiw);
    map.insert("slt", Slt);
    map.insert("slti", Slti);
    map.insert("sltiu", Sltiu);
    map.insert("sub", Sub);
    map.insert("subw", Subw);
    map.insert("xor", Xor);
    map.insert("xori", Xori);
    map.insert("mul", Mul);
    map.insert("mulw", Mulw);
    map.insert("mulh", Mulh);
    map.insert("mulhsu", Mulhsu);
    map.insert("mulhu", Mulhu);
    map.insert("div", Div);
    map.insert("divw", Divw);
    map.insert("divu", Divu);
    map.insert("rem", Rem);
    map.insert("remw", Remw);
    map.insert("remu", Remu);
    map.insert("remuw", Remuw);
    map.insert("beqz", Beqz);
    map.insert("bneq", Bneq);
    map.insert("j", J);
    map.insert("jr", Jr);
    map.insert("la", La);
    map.insert("li", Li);
    map.insert("mv", Mv);
    map.insert("neg", Neg);
    map.insert("nop", Nop);
    map.insert("not", Not);
    map.insert("ret", Ret);
    map.insert("seqz", Seqz);
    map.insert("snez", Snez);
    map
}