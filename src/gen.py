import os
import sys

keywords = """
Add, Addw, Addi, Addiw,
And, Andi,
Auipc,
Beq, Bge, Bgeu, Blt, Bltu, Bne,
Csrrc, Srcrci, Csrrs, Srcrsi, Csrrw, Csrrwi,
Ecall, Ebreak,
Jal, Jalr,
Lb, Lbu, Ld, Lh, Lhu, Lui, Lw, Lwu,
Or, Ori,
Sb, Sd, Sh, Sw,
Sll, Sllw, Slli, Slliw,
Srl, Srlw, Srli, Srliw,
Sra, Sraw, Srai, Sraiw,
Slt, Slti, Sltiu,
Sub, Subw,
Xor, Xori,
Mul, Mulw, Mulh, Mulhsu, Mulhu,
Div, Divw, Divu,
Rem, Remw, Remu, Remuw,

Beqz, Bneq,
J, Jr,
La, Li,
Mv,
Neg,
Nop,
Not,
Ret,
Seqz, Snez,
"""

def pri(indent, *args, **kwargs):
    print(" " * indent, end="")
    print(*args, **kwargs)

def gen_enum(name, variants):
    print("enum {} {{".format(name))
    for name, *fields in variants:
        pri(4, "{}".format(name), end="")
        if len(fields) > 0:
            if type(fields[0]) == dict:
                pri(4, " { ", end="")
                last = len(fields[0]) - 1
                for i, (k, v) in enumerate(fields[0].items()):
                    sep = "," if i != last else ""
                    print("{}: {}{} ".format(k, v, sep), end="")
                print("},")
            else:
                print("(", end="")
                last = len(fields) - 1
                for i, field in enumerate(fields):
                    sep = ", " if i != last else ""
                    print("{}{}".format(field, sep), end="")
                print("),")
        else:
            print(",")
            
    print("}")


kws = [s.strip() for s in keywords.split(",") if not s.isspace()]
def gen_kw_map():
    for kw in kws:
        print("map.insert(\"{}\", {});".format(kw.lower(), kw))

def main():

    gen_enum("Instr", [
        ("Add", "Reg", "Reg", "Reg"),
    ])

if __name__ == '__main__':
    main()
