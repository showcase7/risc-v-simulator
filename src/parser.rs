use std::iter::{Peekable};

struct AsmError {
    pub line: usize,
    pub start: usize, // rel to line start
    pub end: usize, // rel to line start
    pub message: String,
}

type ParseError = Vec<AsmError>;
type ParseResult<T> = Result<T, ParseError>;

struct Parser<'src> {
    errors: Vec<AsmError>,
    text: &'src str,
    tokens: Peekable<Lexer<'src>>,
}

impl Parser {
    pub fn new(source: &str) -> Parser {
        let lexer = Lexer::new(source);
        Parser {
            errors: Vec::new(),
            tokens: lexer.peekable(),
        }
    }
    
    #[inline]
    fn has_error(&self) -> bool {
        ! self.errors.is_empty()
    }
    
    fn is_at_end(&mut self) -> bool {
        if let Some(TokenKind::Eof) = self.tokens.peek().map(|t| t.kind) {
            true
        } else {
            false
        }
    }
}
