
use instructions::{Instr, Reg};
use byteorder::{ReadBytesExt, WriteBytesExt, BE};

pub enum Signal {
    /// Nothing happened.
    Null,
    /// The program has ended.
    End,
    /// An ebreak was called.
    EBreak,
    /// The exit ecall was called.
    Exit(i32),
}

// Prepare for an eventual 64bit impl.
const WORD: i32 = 0xFFFFFFFFu32 as i32;

#[inline]
fn word(value: i32) -> i32 {
    // TODO: Does the word versions also only write to the rightmost 32 bits of a register?
    value & WORD
}

pub type ExeResult<T> = Result<T, String>;

pub struct Interpreter<'ins> {
    registers: [i32; 32],
    pc: u32,
    program: &'ins [Instr],
    memory: Vec<u8>,
    end: u32,
    done: bool,
}
impl<'ins> Interpreter<'ins> {
    pub fn new(program: &'ins[Instr], memory: Vec<u8>) -> Interpreter<'ins> {
        let mut registers = [0; 32];
        // Initialize the stack pointer
        registers[Reg::Sp.index() as usize] = memory.len() as i32;
        
        Interpreter {
            registers: registers,
            pc: 0,
            program: program,
            memory: memory,
            done: false,
            end: (program.len() * 4) as u32,
        }
    }
    
    fn assign(&mut self, reg: Reg, value: i32) {
        match reg {
            Reg::Zero => {},
            reg => {
                if let Reg::Ra = reg {
                    eprintln!("  Ra = {}", value);
                }
                self.registers[reg.index() as usize] = value;
            }
        }
    }
    
    #[inline]
    pub fn is_done(&self) -> bool {
        self.done
    }

    /// Returns the register state of the simulation.
    pub fn registers(&self) -> [i32; 32] {
        self.registers.clone()
    }
    
    #[inline]
    fn read(&self, reg: Reg) -> i32 {
        self.registers[reg.index() as usize]
    }

    /// Returns the index of the current instruction in the instruction array.
    #[inline]
    fn pc_index(&self) -> usize {
        (self.pc / 4) as usize
    }

    /// Signals that an error happened during the execution of an operation.
    fn error<S: Into<String>, T>(&mut self, message: S) -> ExeResult<T> {
        self.done = true;
        // TODO: format more prettily.
        let op = self.program[self.pc_index()];
        let message = format!("{} (0x{:x}): '{:?}': {}", self.pc_index(), self.pc, op, message.into());
        Err(message)
    }

    /// Validates the given jump address.
    fn validate_jump_addr(&mut self, addr: i32) -> ExeResult<()> {
        if addr < 0 || addr as u32 >= self.end || (addr % 4) != 0 {
            self.error(format!(
                "Invalid jump address: {} (0x{:x})", addr / 4, addr
            ))
        } else {
            Ok(())
        }
    }

    /// Validates that the given load destination register isn't Zero.
    fn validate_load_reg(&mut self, reg: Reg) -> ExeResult<()> {
        if let Reg::Zero = reg {
            self.error(format!(
                "Attempt to load into register zero"
            ))
        } else {
            Ok(())
        }
    }

    /// Validates the given shift amount.
    fn validate_shift(&mut self, amount: i32) -> ExeResult<()> {
        if amount < 0 || amount > 32 {
            self.error(format!(
                "Shift attempted with value {}", amount
            ))
        } else {
            Ok(())
        }
    }

    /// Validates the given source address for a load instruction.
    fn validate_load_addr(&mut self, addr: i32, bytes: usize) -> ExeResult<()> {
        let memsize = self.memory.len();
        if addr < 0 {
            self.error(format!(
                "Load at negative address: {} [mem size: {}B]",
                addr, memsize
            ))
        } else {
            let end = addr as usize + bytes;
            if addr as usize >= self.memory.len() {
                self.error(format!(
                    "Load at out of bounds address: {} [mem size: {}B]",
                    addr, memsize
                ))
            } else if end > self.memory.len() {
                self.error(format!(
                    "Load data is out of bounds: {} + {}B [mem size: {}B]",
                    addr, bytes, memsize
                ))
            } else {
                Ok(())
            }
        }
    }
    
    pub fn advance(&mut self) -> ExeResult<Signal> {
        use instructions::Instr::*;

        if self.pc == self.end {
            self.done = true;
            return Ok(Signal::End);
        }
        
        // NOTE: The pc is incremented by default at the end, meaning that
        // jump instrs need to return in order to avoid this.
        let instr = self.program[self.pc_index()];
        match instr {
            // ====================== Arithmetic ====================== 
            Add { rd, rs1, rs2 } => {
                let res = self.read(rs1).wrapping_add(self.read(rs2));
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} + {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Sub { rd, rs1, rs2} => {
                let res = self.read(rs1).wrapping_sub(self.read(rs2));
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} - {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Sll { rd, rs1, rs2 } => {
                let shift_amount = self.read(rs2);
                self.validate_shift(shift_amount)?;
                let res = self.read(rs1) << shift_amount;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} << {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            },
            Slt { rd, rs1, rs2 } => {
                let res = if self.read(rs1) < self.read(rs2) {1} else {0};
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} < {:?} ({})", self.pc_index(), rd, rs1, rs2, if res == 1 {"true"} else {"false"});
            },
            Sltu { rd, rs1, rs2 } => {
                let res = if (self.read(rs1) as u32) < (self.read(rs2) as u32) {1} else {0};
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} unsigned < {:?} ({})", self.pc_index(), rd, rs1, rs2, if res == 1 {"true"} else {"false"});
            }
            Xor { rd, rs1, rs2 } => {
                let res = self.read(rs1) ^ self.read(rs2);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} ^ {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Srl { rd, rs1, rs2 } => {
                let shift_amount = self.read(rs2);
                self.validate_shift(shift_amount)?;
                let res = (self.read(rs1) as u32 >> shift_amount as u32) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} unarith >> {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Sra { rd, rs1, rs2 } => {
                let shift_amount = self.read(rs2);
                self.validate_shift(shift_amount)?;
                let res = self.read(rs1) >> shift_amount;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} >> {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Or { rd, rs1, rs2 } => {
                let res = self.read(rs1) | self.read(rs2);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} | {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            },
            And { rd, rs1, rs2 } => {
                let res = self.read(rs1) & self.read(rs2);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} & {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            },
            Addw { rd, rs1, rs2 } => {
                let res = word(self.read(rs1)).wrapping_add(word(self.read(rs2) ));
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} word + {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            },
            Subw { rd, rs1, rs2 } => {
                let res = word(self.read(rs1)) - word(self.read(rs2));
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} word - {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Sllw { rd, rs1, rs2 } => {
                let shift_amount = word(self.read(rs2));
                self.validate_shift(shift_amount)?;
                let res = word(self.read(rs1)) << shift_amount;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} word << {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Srlw { rd, rs1, rs2 } => {
                let shift_amount = word(self.read(rs2));
                self.validate_shift(shift_amount)?;
                let res = ((word(self.read(rs1)) as u32) >> (shift_amount as u32)) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} word unarith >> {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Sraw { rd, rs1, rs2 } => {
                let shift_amount = word(self.read(rs2));
                self.validate_shift(shift_amount)?;
                let res = word(self.read(rs1)) >> shift_amount;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} word >> {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }

            // ========================= Mul/Div ===========================
            Mul { rd, rs1, rs2 } => {
                let mul_res = self.read(rs1) as i64 * self.read(rs2) as i64;
                let res = (mul_res & 0xFFFF_FFFF) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} * {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Mulh { rd, rs1, rs2 } => {
                let mul_res = self.read(rs1) as i64 * self.read(rs2) as i64;
                let res = (mul_res >> 32) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} mulh {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Mulhsu { rd, rs1, rs2 } => {
                let r1 = self.read(rs1) as i64;
                let mut r2 = self.read(rs2) as i64;
                if r2 < 0 {
                    r2 += 1 << 32
                }
                let mul_res = r1 * r2;
                let res = (mul_res >> 32) as i32;
                
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} mulhsu {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Mulhu { rd, rs1, rs2 } => {
                // Apparently casting i32 directly to u64 results in a
                // cast through i64, ie: sign-extending for negative values.
                // Not too great for this use case.
                let r1 = self.read(rs1) as u32 as u64;
                let r2 = self.read(rs2) as u32 as u64;
                let mul_res = r1 * r2;
                let res = (mul_res >> 32) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} mulhu {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Div { rd, rs1, rs2 } => {
                let r1 = self.read(rs1);
                let r2 = self.read(rs2);
                if r2 == 0 {
                    return self.error("Division by zero");
                }
                let res = r1 / r2;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} / {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Divu { rd, rs1, rs2 } => {
                let r1 = self.read(rs1) as u32;
                let r2 = self.read(rs2) as u32;
                if r2 == 0 {
                    return self.error("Division by zero");
                }
                let res = r1 / r2;
                self.assign(rd, res as i32);
                eprintln!("{:04}: {:?} = {:?} uns. / {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Rem { rd, rs1, rs2 } => {
                let res = self.read(rs1) % self.read(rs2);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} % {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }
            Remu { rd, rs1, rs2 } => {
                let res = self.read(rs1) as u32 % self.read(rs2) as u32;
                self.assign(rd, res as i32);
                eprintln!("{:04}: {:?} = {:?} uns. % {:?} ({})", self.pc_index(), rd, rs1, rs2, res);
            }

            // =================== Arithmetic immediate ==================== 
            Addi { rd, rs1, imm } => {
                let res = self.read(rs1).wrapping_add(imm);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} + {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Slti { rd, rs1, imm } => {
                let res = if self.read(rs1) < imm {1} else {0};
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} < {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Sltiu { rd, rs1, imm } => {
                let res = if (self.read(rs1) as u32) < imm {1} else {0};
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} unsigned < {} ({})", self.pc_index(), rd, rs1, imm, res);
            }

            Slli { rd, rs1, imm } => {
                let res = self.read(rs1) << imm;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} << {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Srli { rd, rs1, imm } => {
                let res = ((self.read(rs1) as u32) >> (imm as u32)) as i32;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} unarith >> {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Srai { rd, rs1, imm } => {
                let res = self.read(rs1) >> imm;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} >> {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Andi { rd, rs1, imm } => {
                let res = self.read(rs1) & imm;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} & {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Ori { rd, rs1, imm } => {
                let res = self.read(rs1) | imm;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} >> {} ({})", self.pc_index(), rd, rs1, imm, res);
            }
            Xori { rd, rs1, imm } => {
                let res = self.read(rs1) ^ imm;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {:?} >> {} ({})", self.pc_index(), rd, rs1, imm, res);
            }

            // ======================== Immediate ========================== 
            Lui { rd, imm } => {
                let res = imm << 12;
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = lui {} = {}", self.pc_index(), rd, imm, res);
            }
            Auipc { rd, imm } => {
                let res = (self.pc as i32) + (imm << 12);
                self.assign(rd, res);
                eprintln!("{:04}: {:?} = {} aiupc {} = {}", self.pc_index(), rd, self.pc, imm << 12, res);
            }

            // =========================== Branch ===========================
            Beq { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if self.read(rs1) == self.read(rs2) {
                    eprintln!("{:04}: Beq to {} (pc -> {})\n", self.pc_index(), imm, target);
                    self.pc = target as u32;
                    return Ok(Signal::Null);
                }
            }
            Bne { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if self.read(rs1) != self.read(rs2) {
                    eprintln!("{:04}: Bne to {} (pc -> {})\n", self.pc_index(), imm, target);
                    self.pc = target as u32;
                    return Ok(Signal::Null);
                }
            }
            Bge { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if self.read(rs1) >= self.read(rs2) {
                    self.pc = target as u32;
                    eprintln!("{:04}: Bge to {} (pc -> {})\n", self.pc_index(), imm, target);
                    return Ok(Signal::Null);
                }
            }
            Bgeu { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if self.read(rs1) as u32 >= self.read(rs2) as u32 {
                    eprintln!("{:04}: Bgeu to {} (pc -> {})\n", self.pc_index(), imm, target);
                    self.pc = target as u32;
                    return Ok(Signal::Null);
                }
            }
            Blt { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if self.read(rs1) < self.read(rs2) {
                    eprintln!("{:04}: Blt to {} (pc -> {})\n", self.pc_index(), imm, target);
                    self.pc = target as u32;
                    return Ok(Signal::Null);
                }
            }
            Bltu { rs1, rs2, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                if (self.read(rs1) as u32) < self.read(rs2) as u32 {
                    eprintln!("{:04}: Bltu to {} (pc -> {})\n", self.pc_index(), imm, target);
                    self.pc = target as u32;
                    return Ok(Signal::Null);
                }
            }
            // ====================== Jump and link ========================
            Jalr { rs1, rd, imm } => {
                let target = (self.read(rs1) + imm) as u32 & 0xFF_FF_FF_FE;
                self.validate_jump_addr(target as i32)?;
                let ret_addr = (self.pc + 4) as i32;
                self.assign(rd, ret_addr);
                eprintln!("{:04}: Jalr -> {:?} + {} [{}], {:?} = PC\n", self.pc_index(), rs1, imm, target, rd);
                self.pc = target;
                return Ok(Signal::Null);
            }
            Jal { rd, imm } => {
                let target = self.pc as i32 + imm;
                self.validate_jump_addr(target)?;
                let ret_addr = (self.pc + 4) as i32;
                self.assign(rd, ret_addr);
                eprintln!("{:04}: Jal -> PC + {} [{}], {:?} = PC\n", self.pc_index(), imm, target, rd);
                self.pc = target as u32;
                return Ok(Signal::Null);
            }

            // ========================== Load =============================
            Lb { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 1)?;
                let index = addr as usize;
                let mut value = self.memory[index] as i32;
                if (value & 0x80) != 0 {
                    value -= 1 << 7;
                }
                eprintln!("{:04}: {:?} = lb M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value);
            }
            Lh { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 2)?;
                let index = addr as usize;
                let mut value = (&self.memory[index .. index+2]).read_i16::<BE>().unwrap() as i32;
                if (value & 0x8000) != 0 {
                    value -= 1 << 15;
                }
                eprintln!("{:04}: {:?} = lh M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value);
            }
            Lw { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 4)?;
                let index = addr as usize;
                let mut value = (&self.memory[index .. index+4]).read_i32::<BE>().unwrap() as i32;
                // No need to subtract this, I guess?
                /*if (value & 0x80000000) != 0 {
                    value -= 1 << 31;
                }*/
                eprintln!("{:04}: {:?} = lw M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value);
            }
            // Makes no sense in a 32bit env
            /*Ld { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr)?;
            }*/
            Lbu { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 1)?;
                let index = addr as usize;
                let mut value = self.memory[index];
                eprintln!("{:04}: {:?} = lbu M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value as i32);
            }
            Lhu { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 2)?;
                let index = addr as usize;
                let mut value = (&self.memory[index .. index + 2]).read_u16::<BE>().unwrap();
                eprintln!("{:04}: {:?} = lbu M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value as i32);
            }
            Lwu { rd, rs1, imm } => {
                self.validate_load_reg(rd)?;
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 4)?;
                let index = addr as usize;
                let mut value = (&self.memory[index .. index + 4]).read_u32::<BE>().unwrap();
                eprintln!("{:04}: {:?} = lbu M[{:?} + {}] = {}", self.pc_index(), rd, rs1, imm, value);
                self.assign(rd, value as i32);
            }

            // ========================== Store ============================
            Sb { rs2, rs1, imm } => {
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 1)?;
                let value = (self.read(rs2) & 0x000000FF) as u8;
                self.memory[addr as usize] = value;
                eprintln!("{:04}: M[{:?} + {}] = {:?} = {}", self.pc_index(), rs1, imm, rs2, value);
            }
            Sh { rs2, rs1, imm } => {
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 2)?;
                let value = (self.read(rs2) & 0x0000FFFF) as u16;
                let i = addr as usize;
                (&mut self.memory[i .. i + 2]).write_u16::<BE>(value).unwrap();
                eprintln!("{:04}: M[{:?} + {} : 2] = {:?} = {}", self.pc_index(), rs1, imm, rs2, value);
            }
            Sw { rs2, rs1, imm } => {
                let addr = self.read(rs1) + imm;
                self.validate_load_addr(addr, 4)?;
                let value = self.read(rs2) as u32 & 0xFFFFFFFF;
                let i = addr as usize;
                (&mut self.memory[i .. i + 4]).write_u32::<BE>(value).unwrap();
                eprintln!("{:04}: M[{:?} + {} : 4] = {:?} = {}", self.pc_index(), rs1, imm, rs2, value);
            }
            //Sd { rs2, rs1, imm } => {}

            // ========================== Extra ============================
            Ecall => {
                let op = self.read(Reg::A0);
                let arg = self.read(Reg::A1);
                eprintln!("{:04}: Ecall({}, {})", self.pc_index(), op, arg);
                match op {
                    1 => println!("{}", arg),
                    4 => unimplemented!(), // print string
                    9 => unimplemented!(), // allocate on heap
                    10 => {
                        self.done = true;
                        return Ok(Signal::Exit(0));
                    }
                    11 => println!("{}", ((arg & 0xFF) as u8) as char),
                    17 => {
                        self.done = true;
                        return Ok(Signal::Exit(arg));
                    }
                    other => {
                        println!("Unknown ECALL number: {}", other);
                    }
                }
            }
            Ebreak => {
                return Ok(Signal::EBreak);
            }
            other => {
                unimplemented!("Instruction {:?} is not yet supported", other)
            }
        }

        self.pc += 4;
        
        Ok(Signal::Null)
    }
}


