

//const OP_FENCE: u32 = 0b0001111;
const OP_LOAD: u32      = 0b0000011;
const OP_STORE: u32     = 0b0100011;

const OP_ARITH: u32     = 0b0110011;
const OP_ARITH_I: u32   = 0b0010011;
const OP_ARITH_W: u32   = 0b0111011;
const OP_ARITH_IW: u32  = 0b0011011;

const OP_AUIPC: u32     = 0b0010111;
const OP_LUI: u32       = 0b0110111;

const OP_BRANCH: u32    = 0b1100011;
const OP_JALR: u32      = 0b1100111;
const OP_JAL: u32       = 0b1101111;

const OP_EXTRA: u32     = 0b1110011; // ecall, ebreak and csrr codes

// Funct7 codes
const F7_VARIANT: u32 = 0b0100000;
const F7_MULDIV:  u32 = 0b0000001;

// Funct3 codes
const AR3_ADD:  u32 = 0b000;
const AR3_SLL:  u32 = 0b001;
const AR3_SLT:  u32 = 0b010;
const AR3_SLTU: u32 = 0b011;
const AR3_XOR:  u32 = 0b100;
const AR3_SRL:  u32 = 0b101;
const AR3_OR:   u32 = 0b110;
const AR3_AND:  u32 = 0b111;

/*
const L3_B: u32 = 0b000;
const L3_H: u32 = 0b001;
const L3_W: u32 = 0b010;
const L3_D: u32 = 0b011;
const L3_BU: u32 = 0b100;
const L3_HU: u32 = 0b101;
const L3_WU: u32 = 0b110;
*/

const AI3_ADD:  u32 = 0b000;
const AI3_SLL:  u32 = 0b001;
const AI3_SLT:  u32 = 0b010;
const AI3_SLTU: u32 = 0b011;
const AI3_XOR:  u32 = 0b100;
const AI3_SRL:  u32 = 0b101;
const AI3_OR:   u32 = 0b110;
const AI3_AND:  u32 = 0b111;

const AIW3_ADD: u32 = 0b000;
const AIW3_SLL: u32 = 0b001;
const AIW3_SRL: u32 = 0b101;

const REG_MASK:     u32 = 0b11111;
const SHIFT_MASK:   u32 = 0b11111;

mod muldiv {
    pub const MUL:    u32 = 0b000;
    pub const MULH:   u32 = 0b001;
    pub const MULHSU: u32 = 0b010;
    pub const MULHU:  u32 = 0b011;
    pub const DIV:    u32 = 0b100;
    pub const DIVU:   u32 = 0b101;
    pub const REM:    u32 = 0b110;
    pub const REMU:   u32 = 0b111;   
}

mod branch {
    pub const EQ:  u32 = 0b000;
    pub const NE:  u32 = 0b001;
    pub const LT:  u32 = 0b100;
    pub const GE:  u32 = 0b101;
    pub const LTU: u32 = 0b110;
    pub const GEU: u32 = 0b111;
}

mod load {
    pub const B:  u32 = 0b000;
    pub const H:  u32 = 0b001;
    pub const W:  u32 = 0b010;
    pub const D:  u32 = 0b011;
    pub const BU: u32 = 0b100;
    pub const HU: u32 = 0b101;
    pub const WU: u32 = 0b110;
}

mod store {
    pub const B: u32 = 0b000;
    pub const H: u32 = 0b001;
    pub const W: u32 = 0b010;
    pub const D: u32 = 0b011;
}

const EX_ECALL: u32 = 0b000;
const ECALL_VARIANT: i32 = 0b0000_0000_0001; //... one, I guess


#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Reg {
    Zero,
    Ra,
    Sp,
    Gp,
    Tp,
    T0,
    T1,
    T2,
    Fp,
    S1,
    A0,
    A1,
    A2,
    A3,
    A4,
    A5,
    A6,
    A7,
    S2,
    S3,
    S4,
    S5,
    S6,
    S7,
    S8,
    S9,
    S10,
    S11,
    T3,
    T4,
    T5,
    T6,
}
impl Reg {
    pub fn decode(num: u32) -> Option<Reg> {
        use self::Reg::*;
        Some(match num {
            0  => Zero,
            1  => Ra,
            2  => Sp,
            3  => Gp,
            4  => Tp,
            5  => T0,
            6  => T1,
            7  => T2,
            8  => Fp,
            9  => S1,
            10 => A0,
            11 => A1,
            12 => A2,
            13 => A3,
            14 => A4,
            15 => A5,
            16 => A6,
            17 => A7,
            18 => S2,
            19 => S3,
            20 => S4,
            21 => S5,
            22 => S6,
            23 => S7,
            24 => S8,
            25 => S9,
            26 => S10,
            27 => S11,
            28 => T3,
            29 => T4,
            30 => T5,
            31 => T6,
            _ => return None,
        })
    }
    
    pub fn index(&self) -> u8 {
        use self::Reg::*;
        match *self {
            Zero => 0,
            Ra => 1,
            Sp => 2,
            Gp => 3,
            Tp => 4,
            T0 => 5,
            T1 => 6,
            T2 => 7,
            Fp => 8,
            S1 => 9,
            A0 => 10,
            A1 => 11,
            A2 => 12,
            A3 => 13,
            A4 => 14,
            A5 => 15,
            A6 => 16,
            A7 => 17,
            S2 => 18,
            S3 => 19,
            S4 => 20,
            S5 => 21,
            S6 => 22,
            S7 => 23,
            S8 => 24,
            S9 => 25,
            S10 => 26,
            S11 => 27,
            T3 => 28,
            T4 => 29,
            T5 => 30,
            T6 => 31,
        }
    }

    pub fn name(&self) -> &'static str {
        use self::Reg::*;
        match *self {
            Zero => "zero",
            Ra => "ra",
            Sp => "sp",
            Gp => "gp",
            Tp => "tp",
            T0 => "t0",
            T1 => "t1",
            T2 => "t2",
            Fp => "fp",
            S1 => "s1",
            A0 => "a0",
            A1 => "a1",
            A2 => "a2",
            A3 => "a3",
            A4 => "a4",
            A5 => "a5",
            A6 => "a6",
            A7 => "a7",
            S2 => "s2",
            S3 => "s3",
            S4 => "s4",
            S5 => "s5",
            S6 => "s6",
            S7 => "s7",
            S8 => "s8",
            S9 => "s9",
            S10 => "s10",
            S11 => "s11",
            T3 => "t3",
            T4 => "t4",
            T5 => "t5",
            T6 => "t6",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Instr {
    // Arithmetic
    Add  { rd: Reg, rs1: Reg, rs2: Reg },
    Sub  { rd: Reg, rs1: Reg, rs2: Reg },
    Sll  { rd: Reg, rs1: Reg, rs2: Reg },
    Slt  { rd: Reg, rs1: Reg, rs2: Reg },
    Sltu { rd: Reg, rs1: Reg, rs2: Reg },
    Xor  { rd: Reg, rs1: Reg, rs2: Reg },
    Srl  { rd: Reg, rs1: Reg, rs2: Reg },
    Sra  { rd: Reg, rs1: Reg, rs2: Reg },
    Or   { rd: Reg, rs1: Reg, rs2: Reg },
    And  { rd: Reg, rs1: Reg, rs2: Reg },
    Addw { rd: Reg, rs1: Reg, rs2: Reg },
    Subw { rd: Reg, rs1: Reg, rs2: Reg },
    Sllw { rd: Reg, rs1: Reg, rs2: Reg },
    Srlw { rd: Reg, rs1: Reg, rs2: Reg },
    Sraw { rd: Reg, rs1: Reg, rs2: Reg },

    // Mul/Div
    Mul    { rd: Reg, rs1: Reg, rs2: Reg },
    Mulh   { rd: Reg, rs1: Reg, rs2: Reg },
    Mulhsu { rd: Reg, rs1: Reg, rs2: Reg },
    Mulhu  { rd: Reg, rs1: Reg, rs2: Reg },
    Div    { rd: Reg, rs1: Reg, rs2: Reg },
    Divu   { rd: Reg, rs1: Reg, rs2: Reg },
    Rem    { rd: Reg, rs1: Reg, rs2: Reg },
    Remu   { rd: Reg, rs1: Reg, rs2: Reg },
    
    // Arithmetic immediate
    Addi    { rd: Reg, rs1: Reg, imm: i32 },
    Addiw   { rd: Reg, rs1: Reg, imm: i32 },
    Slti    { rd: Reg, rs1: Reg, imm: i32 },
    Sltiu   { rd: Reg, rs1: Reg, imm: u32 },

    Slli    { rd: Reg, rs1: Reg, imm: u32 },
    Slliw   { rd: Reg, rs1: Reg, imm: u32 },
    Srli    { rd: Reg, rs1: Reg, imm: u32 },
    Srliw   { rd: Reg, rs1: Reg, imm: u32 },
    Srai    { rd: Reg, rs1: Reg, imm: u32 },
    Sraiw   { rd: Reg, rs1: Reg, imm: u32 },

    Andi    { rd: Reg, rs1: Reg, imm: i32 },
    Ori     { rd: Reg, rs1: Reg, imm: i32 },
    Xori    { rd: Reg, rs1: Reg, imm: i32 },
    
    Lui     { rd: Reg, imm: i32 },
    Auipc   { rd: Reg, imm: i32 },

    // Branch
    Beq     { rs1: Reg, rs2: Reg, imm: i32 },
    Bge     { rs1: Reg, rs2: Reg, imm: i32 },
    Bgeu    { rs1: Reg, rs2: Reg, imm: i32 },
    Blt     { rs1: Reg, rs2: Reg, imm: i32 },
    Bltu    { rs1: Reg, rs2: Reg, imm: i32 },
    Bne     { rs1: Reg, rs2: Reg, imm: i32 },

    Jal     { rd: Reg, imm: i32 },
    Jalr    { rd: Reg, rs1: Reg, imm: i32 },

    // Load
    Lb  { rd: Reg, rs1: Reg, imm: i32 },
    Lh  { rd: Reg, rs1: Reg, imm: i32 },
    Lw  { rd: Reg, rs1: Reg, imm: i32 },
    Ld  { rd: Reg, rs1: Reg, imm: i32 },
    Lbu { rd: Reg, rs1: Reg, imm: i32 },
    Lhu { rd: Reg, rs1: Reg, imm: i32 },
    Lwu { rd: Reg, rs1: Reg, imm: i32 },

    // Store
    Sb { rs1: Reg, rs2: Reg, imm: i32 },
    Sh { rs1: Reg, rs2: Reg, imm: i32 },
    Sw { rs1: Reg, rs2: Reg, imm: i32 },
    Sd { rs1: Reg, rs2: Reg, imm: i32 },

    // Extra
    Ecall,
    Ebreak,
}

#[inline]
fn decode_rs1(code: u32) -> Reg {
    Reg::decode((code >> 15) & REG_MASK).unwrap()
}

#[inline]
fn decode_rs2(code: u32) -> Reg {
    Reg::decode((code >> 20) & REG_MASK).unwrap()
}

#[inline]
fn decode_rd(code: u32) -> Reg {
    Reg::decode((code >> 7 ) & REG_MASK).unwrap()
}

#[inline]
fn decode_funct7(code: u32) -> u32 {
    (code >> 25) & 0b111_1111
}

#[inline]
fn decode_funct3(code: u32) -> u32 {
    (code >> 12) & 0b111
}

fn decode_r(code: u32) -> (u32, Reg, Reg, u32, Reg) {
    let funct7 = decode_funct7(code);
    let funct3 = decode_funct3(code);
    let rs2 = decode_rs2(code);
    let rs1 = decode_rs1(code);
    let rd  = decode_rd(code);
    (funct7, rs2, rs1, funct3, rd)
}

fn decode_i(code: u32) -> (i32, u32, Reg, u32, Reg, u32) {
    // It matters that the sign is read before shifting
    //println!("I code: 0b{:032b}, imm: 0b{:012b}", code, code >> 20);
    let imm = (code as i32) >> 20;
    let shift_imm = (code >> 20) & SHIFT_MASK;
    let funct7 = decode_funct7(code);
    let rs1 = decode_rs1(code);
    let funct3 = decode_funct3(code);
    let rd = decode_rd(code);
    (imm, shift_imm, rs1, funct3, rd, funct7)
}

fn decode_u(code: u32) -> (i32, Reg) {
    let imm = code as i32 >> 12;
    let rd = decode_rd(code);
    (imm, rd)
}

fn decode_uj(code: u32) -> (Reg, i32) {
    //eprintln!("Decoding JAL code: 0b{:032b}", code);
    const M_SIGN:   u32 = 0x80_00_00_00;
    const M_10_1:   u32 = 0x7F_E0_00_00;
    const M_11:     u32 = 0x00_10_00_00;
    const M_19_12:  u32 = 0x00_0F_F0_00;

    let mut imm  = (code & M_10_1) >> 20;
    imm         |= (code & M_11) >> 9;
    imm         |=  code & M_19_12;
    let mut imm = imm as i32;
    if (code & M_SIGN) != 0 {
        imm -= 1 << 20;
    }
    
    let rd = decode_rd(code);
    (rd, imm)
}

fn decode_sb(code: u32) -> (i32, Reg, Reg, u32) {
    let mut imm  = (code & 0x7E000000) >> 20;
    imm         |= (code & 0x00000F00) >> 7;
    imm         |= (code & 0x00000080) << 4;
    
    let mut imm = imm as i32;

    // Make the immediate value signed
    if (code & 0x80000000) != 0 {
        imm -= 1 << 12;
    }

    let funct3 = decode_funct3(code);
    let rs2 = decode_rs2(code);
    let rs1 = decode_rs1(code);
    (imm, rs2, rs1, funct3)
}

fn decode_s(code: u32) -> (i32, Reg, Reg, u32) {
    let mut imm  = (code & 0xFE000000) as i32 >> 20;
            imm |= ((code >> 7) & 0b11111) as i32;
    let funct3 = decode_funct3(code);
    let rs2 = decode_rs2(code);
    let rs1 = decode_rs1(code);
    (imm, rs2, rs1, funct3)
}



impl Instr {
    pub fn decode(code: u32) -> Result<Instr, String> {
        use self::Instr::*;
        //println!("Code: 0b{:032b}", code);
        let opcode = code & 0b111_1111;
        Ok(match opcode {
            OP_LOAD => {
                use self::load::*;
                let (imm, _, rs1, funct3, rd, _) = decode_i(code);
                match funct3 {
                    B  => Lb  { rd, rs1, imm },
                    H  => Lh  { rd, rs1, imm },
                    W  => Lw  { rd, rs1, imm },
                    D  => Ld  { rd, rs1, imm },
                    BU => Lbu { rd, rs1, imm },
                    HU => Lhu { rd, rs1, imm },
                    WU => Lwu { rd, rs1, imm },
                    _ => {
                        return Err(format!(
                            "LOAD: Invalid funct3 code: {:03b}", funct3
                        ));
                    }
                }
            }
            OP_STORE => {
                use self::store::*;
                let (imm, rs2, rs1, funct3) = decode_s(code);
                match funct3 {
                    B => Sb { rs2, rs1, imm },
                    H => Sh { rs2, rs1, imm },
                    W => Sw { rs2, rs1, imm },
                    D => Sd { rs2, rs1, imm },
                    _ => {
                        return Err(format!(
                            "STORE: Invalid funct3 code: {:03b}", funct3
                        ));
                    }
                }
            }
            OP_ARITH => {
                use self::muldiv::*;
                let (funct7, rs2, rs1, funct3, rd) = decode_r(code);
                match (funct3, funct7) {
                    (AR3_ADD,           0) => Add  { rd, rs1, rs2 },
                    (AR3_ADD,  F7_VARIANT) => Sub  { rd, rs1, rs2 },
                    (AR3_SLL,           0) => Sll  { rd, rs1, rs2 },
                    (AR3_SLT,           0) => Slt  { rd, rs1, rs2 },
                    (AR3_SLTU,          0) => Sltu { rd, rs1, rs2 },
                    (AR3_XOR,           0) => Xor  { rd, rs1, rs2 },
                    (AR3_SRL,           0) => Srl  { rd, rs1, rs2 },
                    (AR3_SRL,  F7_VARIANT) => Sra  { rd, rs1, rs2 },
                    (AR3_OR,            0) => Or   { rd, rs1, rs2 },
                    (AR3_AND,           0) => And  { rd, rs1, rs2 },
                    
                    (MUL,    F7_MULDIV) => Mul     { rd, rs1, rs2 },
                    (MULH,   F7_MULDIV) => Mulh    { rd, rs1, rs2 },
                    (MULHSU, F7_MULDIV) => Mulhsu  { rd, rs1, rs2 },
                    (MULHU,  F7_MULDIV) => Mulhu   { rd, rs1, rs2 },
                    (DIV,    F7_MULDIV) => Div     { rd, rs1, rs2 },
                    (DIVU,   F7_MULDIV) => Divu    { rd, rs1, rs2 },
                    (REM,    F7_MULDIV) => Rem     { rd, rs1, rs2 },
                    (REMU,   F7_MULDIV) => Remu    { rd, rs1, rs2 },
                    
                    _ => {
                        return Err(format!(
                            "Invalid R-Type func codes: {:03b} | {:07b}",
                            funct3, funct7
                        ));
                    }
                }
            }
            OP_ARITH_W => {
                let (funct7, rs2, rs1, funct3, rd) = decode_r(code);
                match (funct3, funct7) {
                    (AR3_ADD, 0         ) => Addw {rd, rs1, rs2},
                    (AR3_ADD, F7_VARIANT) => Subw {rd, rs1, rs2},
                    (AR3_SLL, 0         ) => Sllw {rd, rs1, rs2},
                    (AR3_SRL, 0         ) => Srlw {rd, rs1, rs2},
                    (AR3_SRL, F7_VARIANT) => Sraw {rd, rs1, rs2}, 
                    _ => {
                        return Err(format!(
                            "Invalid R-Type func codes: {:03b} | {:07b}",
                            funct3, funct7
                        ));
                    }
                }
            }
            OP_ARITH_I => {
                // TODO: How does variants in the imm value work?
                let (imm, shift_imm, rs1, funct3, rd, funct7) = decode_i(code);
                match (funct3, funct7) {
                    (AI3_ADD,           _) => Addi  {rd, rs1, imm},
                    (AI3_SLL,           0) => Slli  {rd, rs1, imm: shift_imm},
                    (AI3_SLT,           _) => Slti  {rd, rs1, imm},
                    (AI3_SLTU,          _) => {
                        let imm = code >> 20;
                        Sltiu {rd, rs1, imm}
                    }
                    (AI3_XOR,           _) => Xori  {rd, rs1, imm},
                    (AI3_SRL,           0) => Srli  {rd, rs1, imm: shift_imm},
                    (AI3_SRL,  F7_VARIANT) => Srai  {rd, rs1, imm: shift_imm},
                    (AI3_OR,            _) => Ori   {rd, rs1, imm},
                    (AI3_AND,           _) => Andi  {rd, rs1, imm},
                    _ => {
                        return Err(format!(
                            "Invalid I-Type func codes: {:03b} | {:07b}",
                            funct3, funct7
                        ));
                    }
                }
            }
            OP_ARITH_IW => {
                let (imm, shift_imm, rs1, funct3, rd, funct7) = decode_i(code);
                match (funct3, funct7) {
                    (AIW3_ADD, _) => Addiw { rd, rs1, imm },
                    (AIW3_SLL, 0) => Slliw { rd, rs1, imm: shift_imm },
                    (AIW3_SRL, 0) => Srliw { rd, rs1, imm: shift_imm },
                    (AIW3_SRL, F7_VARIANT) => Sraiw { rd, rs1, imm: shift_imm },
                    _ => {
                        return Err(format!(
                            "Invalid I-Type func codes: {:03b} | {:07b}",
                            funct3, funct7
                        ));
                    }
                }
            }
            OP_AUIPC => {
                //eprintln!("Decoding AUIPC: {:032b}", code);
                let (imm, rd) = decode_u(code);
                Auipc { rd, imm }
            }
            OP_LUI => {
                let (imm, rd) = decode_u(code);
                Lui { rd, imm }
            }
            OP_BRANCH => {
                use self::branch::*;
                let (imm, rs2, rs1, funct3) = decode_sb(code);
                match funct3 {
                    EQ => Beq { rs1, rs2, imm },
                    NE => Bne { rs1, rs2, imm },
                    LT => Blt { rs1, rs2, imm },
                    GE => Bge { rs1, rs2, imm },
                    LTU => Bltu { rs1, rs2, imm },
                    GEU => Bgeu { rs1, rs2, imm },
                    _ => {
                        return Err(format!(
                            "Invalid SB-Type funct3 code: {:03b}",
                            funct3
                        ));
                    }
                }
            }
            OP_JALR => {
                //eprintln!("Decoding JALR: {:032b}", code);
                let (imm, _, rs1, _, rd, _) = decode_i(code);
                Jalr { rd, rs1, imm } 
            }
            OP_JAL => {
                //eprintln!("Decoding JAL: {:032b}", code);
                let (rd, imm) = decode_uj(code);
                Jal { rd, imm }
            }
            OP_EXTRA => {
                let (imm, _shift_imm, _rs1, funct3, _rd, funct7) = decode_i(code);
                match (funct3, imm) {
                    (EX_ECALL, 0) => Ecall,
                    (EX_ECALL, ECALL_VARIANT) => Ebreak,
                    // TODO csrr
                    _ => {
                        return Err(format!(
                            "Invalid EXTRA I-Type func codes: {:03b} | {:07b}",
                            funct3, funct7
                        ));
                    }                    
                }
            }
            _ => {
                return Err(format!(
                    "Opcode {:07b} not yet supported! (instr: {:032b})", 
                    opcode, code
                ));
            }
        })
    }
}

