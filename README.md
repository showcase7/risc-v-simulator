# RISC-V simulator
## Description
This project contains a simulator for the RV32I base instruction subset of the RISC-V instruction set.

The program takes as input a 

The project was made as part of a course in computer architecture.

## Building and running the program
The program can be run by the following 

`$ cargo run --release -- <riscv-file> -o output.res`
(The ’--’ after release is to tell cargo not to eat the CLI arguments)

Alternatively:

`$ cargo build --release`
`$ ./target/release/whisky <riscv-file> -o output.res`

The relevant flags are as follows:

`-o , --output-file <output_file>`
Dumps the registers to the given file after running the instructions

`-b , --dump-binary`
Dumps the registers in binary format instead of a text format

`-h , --help`
Prints the options in the terminal

An example invocation of the program can be done by
`$ cargo run --release -- tests/asm/complex_my_mult.bin -o complex.txt && cat complex.txt`.
This runs the simulator on the compiled RISC-V object code in `tests/asm/complex_my_mult.bin`, which as seen in `tests/asm/complex_my_mult.s` multiplies two complex numbers together. It then outputs the state of its internal registers to `complex.txt`.


## License
This project is available under the Universal Permissive License, Version 1.0 ([LICENSE.txt]) or https://oss.oracle.com/licenses/upl/ .
