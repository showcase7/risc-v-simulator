# complex_my_mult.s
# Multiplies two complex numbers together, using a manual multiplication 
# function

        .data
aa:     .word 10    # Re part of z
bb:     .word 5     # Im part of z

cc:     .word 4     # Re part of w
dd:     .word 7     # Im part of w

        .text
        .globl main

main:
        lw   a0, aa
        lw   a1, bb
        lw   a2, cc
        lw   a3, dd
        jal  complex_mul # Multiply z and w
        nop
        
        # Print the result
        mv   t0, a0
        mv   t1, a1
        li   a0, 1
        mv   a1, t0
        ecall
        li   a0, 11
        li   a1, 32
        ecall
        li   a0, 1
        mv   a1, t1
        ecall
        
        j    end
        nop

        # complex_mul(re_z, im_z, re_w, im_w) -> (re, im)
        # Multiplies two complex numbers together
        # z1 * z2 = (a,b) * (c, d) = (ac - bd, ad + bc)
complex_mul: 
        # I need to save the four arguments between register calls, along with
        # the two products, and the return address
        addi sp, sp, -28 # Make room for 7 words
        sw   ra, 24 (sp)
        # Store callee preserved registers
        sw   s1, 20 (sp)
        sw   s2, 16 (sp)
        sw   s3, 12 (sp)
        sw   s4, 8 (sp)
        sw   s5, 4 (sp)
        sw   s6, 0 (sp)
        
        # Store the arguments in the saved registers
        mv   s1, a0
        mv   s2, a1
        mv   s3, a2
        mv   s4, a3
        
        # Multiply the values using my_mult
        mv   a0, s1
        mv   a1, s3
        jal  my_mult # a0 = a * c
        mv   s5, a0 # re = ac
        
        mv   a0, s2
        mv   a1, s4
        jal  my_mult # a0 = b * d
        sub  s5, s5, a0 # re -= bd
        
        mv   a0, s1
        mv   a1, s4
        jal  my_mult # a0 = a * d
        mv   s6, a0    # im = ad
        
        mv   a0, s2
        mv   a1, s3
        jal  my_mult # a0 = b * c
        add  s6, s6, a0 # im += bc
        
        # Move saved values to return registers
        mv   a0, s5 # ret0 = re
        mv   a1, s6 # ret1 = im
        
        # Restore the saved registers from stack
        lw   s6, 0 (sp)
        lw   s5, 4 (sp)
        lw   s4, 8 (sp)
        lw   s3, 12 (sp)
        lw   s2, 16 (sp)
        lw   s1, 20 (sp)
        lw   ra, 24 (sp)
        addi sp, sp, 28
        
        ret

        # my_mult(a, b) -> product
        # Multiplies two numbers together
my_mult:
        slt  t0, a0, zero # aneg = a < 0
        slt  t1, a1, zero # bneg = a < 0
        xor  t2, t0, t1 # negative_product = aneg xor bneg
        li   t3, 0 # product = 0
        li   t4, 0 # i = 0
        li   t5, 32 # const 32
        bnez t0, negate_a # if aneg is not false: negate_a
        bnez t1, negate_b # if bneg is not false: negate_b
        j    mul_loop
negate_a:
        sub  a0, zero, a0 # neg a0
        beqz t1, mul_loop # if bneg is false: mul_loop
negate_b:
        sub  a1, zero, a1 # neg a1
mul_loop:
        andi t0, a1, 1     # do_multiply = b & 1
        beqz t0, update   # if not do_multiply: goto update
        
        add  t3, t3, a0 # product += a

update:
        addi t4, t4, 1 # i += 1
        slli a0, a0, 1 # a <<= 1
        srli a1, a1, 1 # b >>= 1
        blt  t4, t5, mul_loop # if i < 32, loop again
        beqz t2, my_mult_done # if the product isn't negative, goto done
        
        # Don't check for overflow
        # though we can check that using our 'negative_product' var and
        # comparing that with the sign of the product
        
        
        # Negate the product
        sub  t3, zero, t3
        
my_mult_done:   
        # Return product
        mv   a0, t3
        ret


end:
        nop
        
