        .data
n:      .word 3

        .text
main:
        la   t0, n # addr = n
        lw   a0, 0(t0) # a0 = *addr
        jal  ra, factorial # factorial(n)
   
        addi a1, a0, 0
        addi a0, x0, 1
        ecall # print result

        addi a0, x0, 10
        ecall # exit

factorial:
        li   t0, 1
        blt  a0, t0, ret1 # check whether n lt 1
        
        addi sp, sp, -8
        sw   a0, 4 (sp) # push n to the stack
        sw   ra, 0 (sp) # push retaddr to stack
                
        addi a0, a0, -1 # n -= 1
        jal  ra, factorial # n = factorial(n)
        lw   ra, 0 (sp) # ra = stack.pop()
        lw   t0, 4 (sp) # old_n = stack.pop()
        addi sp, sp, 8
        mul  a0, a0, t0 # n = n * old_n

        jalr x0, ra, 0 # return
ret1:
        li   a0, 1
        jalr x0, ra, 0 # return
