# parse_number.s
# For the Venus riscv-emulator (uses ecalls)
# Parses a number encoded as an ascii string, optionally prefixed with '+' or
# '-'

.data
    good:       .string "+100"
    negative:   .string "-123"
    bad:        .string "bad"
    sign:       .string "-"
    empty:      .string ""

.text
        j    main

parse_number:
        li   t1, 0         # read_number = false (have we read a number char yet)
        lbu  t2, 0 (a0)     # ch = *s
        li   t3, 0         # i = 0
        li   t4, 0         # negative = false
        li   t5, 0         # sum = 0
        beq  x0, t2, error # empty str -> goto err

        # Check if the first char is a sign char
read_sign:
        li   t0, '+'
        beq  t2, t0, after_sign
        li   t0, '-'
        # try to read a number if it isn't a minus
        bne  t2, t0, read_number_char
        li   t4, 1 # negative = true

after_sign:
        # We've just read the first char, so increment i
        li   t3, 1         # i = 1
        add  t0, a0, t3    # addr = s[i]
        lbu  t2, 0 (t0)     # ch = *addr

read_number_char:
        beqz t2, done # no more bytes -> we're done
    
        # Check that '0' <= ch <= '9'
        li   t0, '9'
        bgt  t2, t0, error
        li   t0, '0'
        blt  t2, t0, error
    
        # If it's within the range, we just need to offset it to get the value
        li   t6, 10
        mul  t5, t5, t6    # sum *= 10
        sub  t2, t2, t0    # ch -= '0'
        add  t5, t5, t2    # sum += ch
        addi t3, t3, 1    # i += 1
        add  t0, a0, t3    # addr = s[i]
        lbu  t2, 0 (t0)     # ch = *addr
        li   t1, 1         # read_number = true
        j    read_number_char

done:
        beqz t1, error   # if we haven't read a number yet, it's an error
        beqz t4, end     # if not negative, goto end
    
        # Negate the sum ('neg' is not supported by venus)
        li   t0, 0
        sub  t5, t0, t5
        j    end

error:
        # Return -1
        li   a0, -1
        ret

end:
        # Return the sum
        mv   a0, t5
        ret

main:
        la   a0, good
        jal  ra, parse_number
        # Print the number
        mv   a1, a0
        li   a0, 1
        ecall

        la   a0, negative
        jal  ra, parse_number
        mv   a1, a0
        li   a0, 1
        ecall

        la   a0, bad
        jal  ra, parse_number
        mv   a1, a0
        li   a0, 1
        ecall

        la   a0, sign
        jal  ra, parse_number
        mv   a1, a0
        li   a0, 1
        ecall

        la   a0, empty
        jal  ra, parse_number
        mv   a1, a0
        li   a0, 1
        ecall

