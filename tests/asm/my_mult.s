# my_mult.s
# Multiplies two two's complement encoded words together manually

        .data
a:      .word -17
b:      .word 23

        .text
        .globl main
 
main:
        lw   a0, a
        lw   a1, b
        jal  my_mult # multiply a by b
        
        # Print the result
        mv   a1, a0
        li   a0, 1
        ecall
        
        nop
        j    end
        nop
    
my_mult:
        slt  t0, a0, zero # aneg = a < 0
        slt  t1, a1, zero # bneg = a < 0
        xor  t2, t0, t1 # negative_product = aneg xor bneg
        li   t3, 0 # product = 0
        li   t4, 0 # i = 0
        li   t5, 32 # const 32
        bnez t0, negate_a # if aneg is not false: negate_a
        bnez t1, negate_b # if bneg is not false: negate_b
        j    mul_loop
negate_a:
        sub  a0, zero, a0 # neg a0
        beqz t1, mul_loop # if bneg is false: mul_loop
negate_b:
        sub  a1, zero, a1 # neg a1
mul_loop:
        andi t0, a1, 1     # do_multiply = b & 1
        beqz t0, update   # if not do_multiply: goto update
        
        add  t3, t3, a0 # product += a

update:
        addi t4, t4, 1 # i += 1
        slli a0, a0, 1 # a <<= 1
        srli a1, a1, 1 # b >>= 1
        blt  t4, t5, mul_loop # if i < 32, loop again
        beqz t2, done # if the product isn't negative, goto done
        
        # Don't check for overflow
        # though we can check that using our 'negative_product' var and
        # comparing that with the sign of the product
        
        
        # Negate the product
        sub  t3, zero, t3
        
done:   
        # Return product
        mv   a0, t3
        ret

end:
        nop
