# hello.s
# For the Venus riscv-emulator (uses ecalls)
# Prints a hello world string character by character

.data
hello: .string "Hello world"

.text
        la   a0, hello

print_char:
        mv   t3, a0 # s = arg0

loop:
        add  t1, t3, t0 # addr = s[i]
        lbu  t2, 0 (t1) # ch = *addr
        beq  t2, x0, end # if ch == 0: goto end
        # This is where I could validate that it is a valid ascii char
        li   a0, 11
        mv   a1, t2
        ecall # print_char(ch)
        addi t0, t0, 1 # i += 1
        j    loop # goto print_char

end:
        nop
