# complex.s
# Multiplies two complex numbers together

        .data
aa:     .word 10    # Re part of z
bb:     .word 5     # Im part of z

cc:     .word 4     # Re part of w
dd:     .word 7     # Im part of w

        .text
        .globl main

main:
        lw   a0, aa
        lw   a1, bb
        lw   a2, cc
        lw   a3, dd
        jal  complex_mul # Multiply z and w
        nop
        
        # Print the result
        mv   t0, a0
        mv   t1, a1
        li   a0, 1
        mv   a1, t0
        ecall
        li   a0, 11
        li   a1, 32
        ecall
        li   a0, 1
        mv   a1, t1
        ecall
        
        j    end
        nop

        # z1 * z2 = (a,b) * (c, d) = (ac - bd, ad + bc)
complex_mul:
        mul  t0, a0, a2   # ac = a * c
        mul  t1, a1, a3   # bd = b * d
        sub  t0, t0, t1   # re = ac - bd
        mul  t1, a0, a3   # ad = a * d
        mul  t2, a1, a2   # bc = b * c
        add  a1, t1, t2   # ret1 = ad + bc
        mv   a0, t0       # ret0 = re
        ret

end:
        nop
        
