lui t0 44995
addi t0 t0 -1366
addi t1 zero -1
addi t2 zero 3
mul t1 t1 t2
mulh a1 t0 t1 # ok
mulhsu a2 t0 t1 # incorrect: expected 184298153, found -1
mulhu a3 t0 t1 # incorrect: expected 184298153, found -1
div a4 t0 t1 # ok
divu a5 t0 t1 # ok
rem a6 t0 t1 # ok
remu a7 t0 t1 # ok
addi a0 zero 10
ecall
