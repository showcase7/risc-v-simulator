


bne_code = 0xFE_B5_1E_E3 #0xE3_1E_B5_FE
blt_code = 0xFE_B5_48_E3 #0xE3_48_B5_FE
jal_code = 0b00000110010000000000000001101111
jal_code2 = 0b00001001010000000000000001101111

print("BNE:", bin(bne_code))
print("BLT:", bin(blt_code))

print("0x80: {}".format(bin(0x80)))
print("0x7E: {}".format(bin(0x7E)))
print("0x0F: {}".format(bin(0x0F)))
SIGN_MASK       = 0x80_00_00_00
FIVE_TEN_MASK   = 0x7E_00_00_00
ONE_FOUR_MASK   = 0x00_00_0F_00
ELEVEN_MASK     = 0x00_00_00_80
print("Masks:")
print("  Sign : {:032b}".format(SIGN_MASK))
print("  5:10 : {:032b}".format(FIVE_TEN_MASK))
print("  1:4  : {:032b}".format(ONE_FOUR_MASK))
print("  11   : {:032b}".format(ELEVEN_MASK))
print("")
print("Shifted positions:")
print("  5:10 : {:012b}".format(FIVE_TEN_MASK >> 20))
print("  1:4  : {:012b}".format(ONE_FOUR_MASK >> 7))
print("  11   : {:012b}".format(ELEVEN_MASK << 4))

print("JAL stuff")
JAL_TEN_ONE_MASK    = 0x7F_E0_00_00
JAL_ELEVEN_MASK     = 0x00_10_00_00
JAL_SIGN_MASK       = 0x80_00_00_00
JAL_NINETEEN_TWELVE = 0x00_0F_F0_00
print("Masks")
print("  Sign  : {:032b}".format(JAL_SIGN_MASK))
print("  10:1  : {:032b}".format(JAL_TEN_ONE_MASK))
print("  11    : {:032b}".format(JAL_ELEVEN_MASK))
print("  19:12 : {:032b}".format(JAL_NINETEEN_TWELVE))
print("")
print("Shifted positions:")
print("  10:1  : {:032b}".format(JAL_TEN_ONE_MASK       >> 20   ))
print("  11    : {:032b}".format(JAL_ELEVEN_MASK        >> 9    ))
print("  19:12 : {:032b}".format(JAL_NINETEEN_TWELVE    >> 0    ))

def imm(code):
    negative = (code & SIGN_MASK) != 0
    five_ten = (code & FIVE_TEN_MASK) >> 20
    one_four = (code & ONE_FOUR_MASK) >> 7
    eleven = (code & ELEVEN_MASK) << 4
    value = five_ten | one_four | eleven
    print("Positive value: {}".format(value))
    if negative:
        value -= 1 << 12
    
    return value

def jump_imm(code):
    negative = (code & JAL_SIGN_MASK) != 0
    ten_one = (code & JAL_TEN_ONE_MASK) >> 20
    eleven = (code & JAL_ELEVEN_MASK) >> 9
    nineteen_twelve = (code & JAL_NINETEEN_TWELVE) >> 0
    value = ten_one | eleven | nineteen_twelve
    print("Positive value: {}".format(value))
    if negative:
        value -= 1 << 20
    
    return value

print("bne imm: {}".format(imm(bne_code)))
print("blt imm: {}".format(imm(blt_code)))
print("jal imm: {}".format(jump_imm(jal_code)))
print("jal2 imm: {}".format(jump_imm(jal_code2)))



