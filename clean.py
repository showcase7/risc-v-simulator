import os
import sys

FOLDER = "tests/asm"
INDENT = 8

INVALID = {"li", "la"}
LOAD_STORE_OPS = set("lb lh lw ld lbu lwu sb sh sw sd".split())
def is_load_store(op):
    return op in LOAD_STORE_OPS

def clean(text):
    lines = []
    modified = False
    in_text = False
    for line in text.splitlines():
        s = line.strip()
        if s.endswith(":"):
            lines.append(line)
            continue

        if not in_text:
            # Replace the venus '.asciiz' with the riscvtools '.string'
            lines.append(line.replace(".asciiz", ".string"))
            if s == ".text":
                in_text = True
        
        else:
            if s == "":
                lines.append(line)
                continue
            
            elif s.startswith("#"):
                lines.append("{}{}".format(" "*INDENT, s))
                continue
            
            line_parts = [" " * (INDENT - 1)]
            op, *args = s.split(" ")
            nof_args = 0
            for arg in args:
                if arg.startswith("#"):
                    break
                elif arg != "":
                    nof_args += 1
            
            line_modified = False
            if len(op) < 4 and args and args[0] != "":
                line_parts.append("{}{}".format(op, (4 - len(op)) * " "))
                line_modified = True
            else:
                line_parts.append(op)
            
            # Add indent for single-op lines too
            if line.find(op) != INDENT:
                line_modified = True
            
            for i, arg in enumerate(args):
                if arg.startswith("#"):
                    line_parts.append(" ".join(args[i:]))
                    break
                
                if arg != "" and not arg.endswith(","):
                    line_modified = True
                    arg += ","
                
                line_parts.append(arg)
            
            #print("Line parts: {}".format(line_parts))
            # Try to remove trailing commas :)
            for i in range(len(line_parts)-1, 0, -1):
                arg = line_parts[i]
                if arg.startswith("#"):
                    continue

                if arg.endswith(","):
                    line_parts[i] = arg[:-1]
                    break
            
            # If it's a load/store op, and we need to fix the address
            if is_load_store(op) and nof_args != 2:
                wrapped = False
                for i in range(len(line_parts)-1, 0, -1):
                    arg = line_parts[i]
                    if arg.startswith("#") or arg == "":
                        continue
                    
                    # Wrap the register in parentheses
                    if not wrapped:
                        # Assume it's already in the right form
                        if arg.startswith("("):
                            break

                        #print("Wrapped {}!".format(arg), file=sys.stderr)
                        line_parts[i] = "({})".format(arg)
                        wrapped = True
                        line_modified = True
                    # Remove the comma from the offset
                    else:
                        line_parts[i] = arg[:-1]
                        break
            
            #print("Line parts: {}".format(line_parts), file=sys.stderr)
            if not line_modified:
                lines.append(line)
            else:
                lines.append(" ".join(line_parts))
            
            
    
    # Add newline for unixnissen
    lines.append("")
    
    return os.linesep.join(lines)

def main():
    #files = ["complex.s"]
    files = os.listdir(FOLDER)
    for filename in files:
        if filename.startswith("."): continue
        if not filename.endswith(".s"): continue
        print("Cleaning {!r}...".format(filename), file=sys.stderr)
        filepath = os.path.join(FOLDER, filename)
        with open(filepath, "r") as f:
            text = f.read()
        
        cleaned = clean(text)
        print("Cleaned:", file=sys.stderr)
        basename = filename.rsplit(".", 1)[0]
        outpath = os.path.join(FOLDER, basename + "_clean.s")
        with open(filepath, "w") as f:
            f.write(cleaned)
        
        #print(cleaned)
        
        print("  Done!", file=sys.stderr)

            

if __name__ == '__main__':
    main()

